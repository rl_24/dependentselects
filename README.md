# JIRA Dependent Selects ScriptRunner Behaviour

A simple JIRA ScriptRunner Behaviour add-on script.

## Usage

* Install the ScriptRunner plugin through the JIRA Administration - Add-ons portal page.
* Create 3 new single select fields from the Custom Fields config page (`secure/admin/ViewCustomFields.jspa`): Vehicle Type, Cars, and Planes
* Associate the created fields to the pages you wish to use them on
* Add a new Behaviour from the Behaviours config page (`/secure/admin/Behaviours!default.jspa`)
* Create an Initialiser script with the following code inserted:
```groovy
def carsField = getFieldByName("Cars")
def planesField = getFieldByName("Planes")

carsField.setRequired(false)
carsField.setHidden(true)
planesField.setRequired(false)
planesField.setHidden(true)
```
* Add the 3 fields you created in the previous step: Vehicle Type, Cars, and Planes, to the Behaviour Fields
* For the Vehicle Type, add a `server-side script` with the folowing code inserted:
```groovy
def typeField = getFieldByName("Vehicle Type")
def carsField = getFieldByName("Cars")
def planesField = getFieldByName("Planes")

def type = typeField.getValue()

if (type == "Cars") {
    carsField.setRequired(true)
    carsField.setHidden(false)
    planesField.setRequired(false)
    planesField.setHidden(true)
} else if (type == "Planes") {
    carsField.setRequired(false)
    carsField.setHidden(true)
    planesField.setRequired(true)
    planesField.setHidden(false)
} else {
    carsField.setRequired(false)
    carsField.setHidden(true)
    planesField.setRequired(false)
    planesField.setHidden(true)
}
```
* Save the behaviour
* Navigate to your page, create a new issue, and watch the magic unfold